<div class="doku doku-phpcode">
	
	<?php if ($field) {
		?><div class="doku-phpcode-field">
			<?php echo $field; ?>
		</div>
	<?php } ?>
	<div class="doku-toolbar">
		<?php echo $links; ?>
	</div>
	<div class="doku-phpcode-code">
		<?php echo $phpcode; ?>
	</div>

</div>	
	